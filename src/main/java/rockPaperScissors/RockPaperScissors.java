package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */


        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("Rock", "Paper", "Scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true) {
            System.out.println("Let's play round " + roundCounter);

            String PlayerChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            Random rand = new Random();
            String ComputerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            System.out.print("Human chose " + PlayerChoice + " computer chose " + ComputerChoice+". ");
            if (PlayerChoice.equals(ComputerChoice)) {
                System.out.println("It's a tie.");
            } else if (PlayerChoice.equals("rock") && ComputerChoice.equals("scissors") ||
                    PlayerChoice.equals("scissors") && ComputerChoice.equals("paper") ||
                    PlayerChoice.equals("paper") && ComputerChoice.equals("rock")) {
                System.out.println("Human win!");
                humanScore ++;

            } else {
                System.out.println("Computer win!");
                computerScore ++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);



        if (! readInput("Do you wish to continue playing? (y/n)? ").equals("y")){
            break;

};
    roundCounter ++;

        }

        System.out.println("Bye bye :)");
        }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
